package org.test;

import javax.servlet.annotation.WebServlet;


import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.*;
import java.util.Arrays;
import java.util.List;


/**
 *
 */
@Theme("mytheme")
@Widgetset("org.test.MyAppWidgetset")
public class MyUI extends UI {
    private Label outputLabel = new Label();
    private Controller controller =null;
    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final VerticalLayout layout = new VerticalLayout();
        
        final TextField name = new TextField();
        name.setCaption("Type your Name here:");

       // controller= new Controller( this);



        Button controllerButton = new Button("Don't click me!!");
        controllerButton.addClickListener( e -> {
            layout.addComponent(new Label("You have clicked and I told you not to do it"));
        });

        Button button = new Button("Click Me");
        button.addClickListener( e -> {
            layout.addComponent(new Label("Thanks " + name.getValue() 
                    + ", it works well!"));

            String labelName = name.getValue();
            Integer I = new Integer(labelName);
            outputLabel.setValue(I.toString());
        });

        // pruebas
        TextField tf = new TextField("My Eventful Field");
        tf.setValue("Initial content");
        tf.setMaxLength(20);

// Counter for input length
        Label counter = new Label();
        counter.setValue(tf.getValue().length() +
                " of " + tf.getMaxLength());


// Display the current length interactively in the counter
        tf.addValueChangeListener(event -> {
            TextField tfl = (TextField) event.getProperty().getValue();
            System.out.println(tfl.getValue());
            //counter.setValue(len + " of " + tf.getMaxLength());
        });

        //tf.setValueChangeMode(ValueChangeMode.EAGER);

        CheckBox checkbox1 = new CheckBox("Box with no Check");
        CheckBox checkbox2 = new CheckBox("Box with a Check");

        checkbox2.setValue(true);

        checkbox1.addValueChangeListener(event ->
                checkbox2.setValue(! checkbox1.getValue()));
        checkbox2.addValueChangeListener(event ->
                checkbox1.setValue(! checkbox2.getValue()));




/*

        List<Person> people = Arrays.asList(
                new Person("Nicolaus Copernicus", 1543),
                new Person("Galileo Galilei", 1564),
                new Person("Johannes Kepler", 1571));

// Create a grid bound to the list
        Grid<Person> grid = new Grid<>();
        grid.setItems(people);

        //add them manually because the method set items it does not work
        grid.addRow( new Person("Nicolaus Copernicus", 1543));
        grid.addRow( new Person("Galileo Galilei", 1564));
        grid.addRow( new Person("Johannes Kepler", 1571));
        grid.addColumn(Person::getName).setCaption("Name");
        grid.addColumn(Person::getBirthYear).setCaption("Year of birth");

        layout.addComponent(grid);
        */



        //Label
        
        layout.addComponents(name, button);
        layout.addComponent(controllerButton);

        layout.addComponents(tf);
        layout.addComponents(checkbox1);
        layout.addComponents(checkbox2);
        layout.setMargin(true);
        layout.setSpacing(true);
        
        setContent(layout);
    }

    public Label getOutputLabel() {
        return outputLabel;
    }

    public void setOutputLabel(Label outputLabel) {
        this.outputLabel = outputLabel;
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
