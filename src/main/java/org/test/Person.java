package org.test;
import java.util.Objects;

public class Person {
    private final String name;
    private final long date;

    public Person(String name, long date) {
        this.name = name;
        this.date = date;
    }
    // how you're supposed to implement equals
    public boolean equals(Object other) {
        if (other == this) return true;
        if (other == null) return false;
        if (other.getClass() != this.getClass()) return false;
        Person that = (Person) other;
        return (this.name.equals(that.name)) && (this.date == that.date);
    }

    public String toString() {
        return name + " " + date;
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, name);
    }
}
